# Poetry zsh plugin

This is a pretty simple plugin to automatically activate and switch Poetry virtual environments in zsh.

# Features

* Automatically switches to a virtualenv for the current directory (or parent) if one exists
* Automatically deactivates the current virtualenv if there is no poetry.lock to be found
* __Does not__ automatically install anything (by design!)

# Installing

`zgen load https://gitlab.com/K900/poetry-zsh-plugin.git` (or use any other package manager)
