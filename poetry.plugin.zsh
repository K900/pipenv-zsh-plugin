function() {
  if (( ! $+commands[poetry] )); then
    return 1
  fi

  __zsh_poetry_hook() {
    setopt localoptions extendedglob

    deactivate_if_needed() {
      if whence deactivate &> /dev/null; then
	      deactivate
      fi
    }

    local poetry_lock=((../)#poetry.lock(N:a))
    if [ -n "${poetry_lock}" ]; then
      if [ "${poetry_lock}" != "${__ZSH_LAST_VENV}" ]; then
        deactivate_if_needed
        local venv=$(poetry env info -p)
        if [ -n "${venv}" ]; then
          source "${venv}/bin/activate"
          __ZSH_LAST_VENV="${poetry_lock}"
        fi
      fi
    else
      deactivate_if_needed
      __ZSH_LAST_VENV=""
    fi
  }

  autoload -Uz add-zsh-hook
  add-zsh-hook chpwd __zsh_poetry_hook
}
